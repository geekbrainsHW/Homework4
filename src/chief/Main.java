package chief;

public class Main {
    public static void main(String[] args) {
        Chief chief = new ChiefAdapter();
        Object key = chief.makeDinner();

        System.out.println(key.toString());
    }
}

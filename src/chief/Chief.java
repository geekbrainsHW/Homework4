package chief;

public interface Chief {
    Object makeBreakfast();

    Object makeDinner();

    Object makeSupper();
}

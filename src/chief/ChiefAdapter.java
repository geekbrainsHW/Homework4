package chief;

public class ChiefAdapter implements Chief {

    private Plumber plumber;

    ChiefAdapter() {
        this.plumber = new Plumber();
    }


    @Override
    public Object makeBreakfast() {
        return plumber.getKey();
    }

    @Override
    public Object makeDinner() {
        return plumber.getScrewDriver();
    }

    @Override
    public Object makeSupper() {
        return plumber.getPipe();
    }
}

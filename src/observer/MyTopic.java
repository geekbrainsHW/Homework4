package observer;

import java.util.ArrayList;
import java.util.List;

public class MyTopic implements Subject {

    private List<Observer> observers;
    private String message;
    private boolean changed;

    MyTopic() {
        observers = new ArrayList<>();
    }

    @Override
    public void register(Observer observer) {
        if (!observers.contains(observer)) observers.add(observer);
    }

    @Override
    public void unregister(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
        this.changed = false;
    }

    @Override
    public Object getUpdate(Observer observer) {
        return this.message;
    }

    void postMessage(String message) {
        this.message = message;
        System.out.println("A new message in blog: " + this.message);
        this.changed = true;
        notifyObservers();
    }

}

package observer;

public class Main {
    public static void main(String[] args) {
        MyTopic topic = new MyTopic();
        Observer observer1 = new Subscriber("Rick");
        Observer observer2 = new Subscriber("Morty");
        Observer observer3 = new Subscriber("Bet");

        topic.register(observer1);
        topic.register(observer2);
        observer1.setSubject(topic);
        observer2.setSubject(topic);
        topic.postMessage("WoW! Amazing news!");
        observer2.setSubject(null);
        topic.unregister(observer2);
        topic.postMessage("Sad news!");

    }

}

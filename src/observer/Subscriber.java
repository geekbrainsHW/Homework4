package observer;

public class Subscriber implements Observer {
    private String name;
    private Subject topic;

    Subscriber(String name) {
        this.name = name;
    }

    @Override
    public void update() {
        String string = (String) topic.getUpdate(this);
        if (string == null) System.out.println(this.name + " nothing changed");
        else System.out.println(this.name + " : new message: " + string);
    }

    @Override
    public void setSubject(Subject subject) {
        this.topic = subject;
    }

}
